var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
var database;


window.addEventListener("load", setup, false);

function setup() {
    document.getElementById("loginSubmit").addEventListener("click", login, false);
    startDB();
}



function startDB() {
    dataBase = indexedDB.open("main", 1);
    dataBase.onupgradeneeded = function (e) {
        active = dataBase.result;
        object = active.createObjectStore("user", {keyPath: 'email', autoIncrement: false});
        object.createIndex('by_password', 'password');
        object.createIndex('by_name', 'name');
        object.createIndex('by_username', 'username');
        object.createIndex('by_movil', 'movil');
        object.createIndex('by_imgName', 'imgName');
        object.createIndex('by_genero', 'genero');
        object.createIndex('by_nacimiento', 'nacimiento');

    };

    dataBase.onsuccess = function (e) {
        alert('Base de datos cargada correctamente');
    };

    dataBase.onerror = function (e) {
        alert('Error cargando la base de datos');
    };
}


function login(e) {
    var active = dataBase.result;
    var data = active.transaction("user", "readonly");
    var object = data.objectStore("user");
    var email = document.getElementById("loginEmail").value;
    var password = document.getElementById("loginPassword").value;
    var request = object.get(email);

    request.onsuccess = function () {
        var result = request.result;
        if (result.password === password) {
            alert("Logueado");
            var history = JSON.parse(localStorage.getItem('history'));
            if (history === null) {
                history = [];
                history.push(email);
                localStorage.setItem('history', JSON.stringify(history));
            }
        }
    };
    request.onerror = function () {
        alert("Usuario o contraseña incorrectas");
    };

}