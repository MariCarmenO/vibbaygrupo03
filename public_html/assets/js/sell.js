var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
var category;

window.addEventListener("load", setup, false);

function setup() {
    document.sellForm.addEventListener("submit", submit, false);
    document.getElementById("dropdownCategory").addEventListener("click", handleCategory, false);
    startDB();
}


function startDB() {
    dataBase = indexedDB.open("main", 1);
    dataBase.onupgradeneeded = function (e) {
        active = dataBase.result;
        object = active.createObjectStore("product", {keyPath: 'id', autoIncrement: true});
        object.createIndex('by_title', 'title');
        object.createIndex('by_description', 'description');
        object.createIndex('by_sellerID', 'sellerID');
        object.createIndex('by_imgName', 'imgName');
        object.createIndex('by_datetime', 'datetime');
        object.createIndex('by_isClosed', 'isClosed');
        object.createIndex('by_price', 'price');
    };

    dataBase.onsuccess = function (e) {
        alert('Base de datos cargada correctamente');
    };

    dataBase.onerror = function (e) {
        alert('Error cargando la base de datos');
    };
}


function submit() {
    var active = dataBase.result;
    var data = active.transaction("product", "readwrite");
    var object = data.objectStore("product");

    var request = object.put({
        title: document.querySelector("#title").value,
        description: document.querySelector("#description").value,
        //sellerID: document.querySelector("#surname").value,
        //imgName: document.querySelector("#description").value,
        datetime: new Date().getTime(),
        isClosed: false,
        price: document.querySelector("#price").value
    });

    request.onerror = function (e) {
        alert(request.error.name + '\n\n' + request.error.message);
    };

    data.oncomplete = function (e) {
        alert('Objeto agregado correctamente');
    };
    

}

function handleCategory(e) {
    var elemento = e.target;
    if (elemento === document.getElementById("catElectronics")) { //-----Price----------
        category = "Electronics";
    } else if (elemento === document.getElementById("catFashion")) { //-----Date----------
        category = "Fashion";
    } else if (elemento === document.getElementById("catMotors")) { //-----Default----------
        category = "Motors";
    }
    
    document.getElementById("dropdownCategory").innerHTML = category + '<span class="caret"></span>';
}
