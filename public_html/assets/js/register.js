var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

window.addEventListener("load", startDB, false);
window.addEventListener("load", setup, false);

function setup() {
    document.sellForm.addEventListener("submit", submit, false);
    startDB();
}


function startDB() {
    dataBase = indexedDB.open("main", 1);
    dataBase.onupgradeneeded = function (e) {
        active = dataBase.result;
        object = active.createObjectStore("user", {keyPath: 'email', autoIncrement: false});
        object.createIndex('by_password', 'password');
        object.createIndex('by_name', 'name');
        object.createIndex('by_username', 'username');
        object.createIndex('by_movil', 'movil');
        object.createIndex('by_imgName', 'imgName');
        object.createIndex('by_genero', 'genero');
        object.createIndex('by_nacimiento', 'nacimiento');
   
    };

    dataBase.onsuccess = function (e) {
        //alert('Base de datos cargada correctamente');
    };

    dataBase.onerror = function (e) {
        //alert('Error cargando la base de datos');
    };
}


function submit() {
    var active = dataBase.result;
    var data = active.transaction("user", "readwrite");
    var object = data.objectStore("user");

    var request = object.put({
        name: document.querySelector("#name").value,
        username: document.querySelector("#username").value,
        movil: document.querySelector("#movil").value,
        password: document.querySelector("#password").value,
        email: document.querySelector("#email").value,
        imgName: document.querySelector("#imgName").value,
        genero: document.querySelector("#genero").value,
        nacimiento: document.querySelector("#nacimiento").value
    });

    request.onerror = function (e) {
        alert(request.error.name + '\n\n' + request.error.message);
    };

    data.oncomplete = function (e) {
        alert('Objeto agregado correctamente');
    };   
}