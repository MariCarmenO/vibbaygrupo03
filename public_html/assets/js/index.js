var dataBase;
var orderBy = "default";
var orderDescending = true;

window.addEventListener("load", setup, false);


function setup() {
    document.getElementById("orderByPrice").addEventListener("click", handleOrder, false);
    document.getElementById("orderByDate").addEventListener("click", handleOrder, false);
    document.getElementById("orderByDefault").addEventListener("click", handleOrder, false);
    document.getElementById("orderDescending").addEventListener("click", handleOrder, false);
    document.getElementById("orderAscending").addEventListener("click", handleOrder, false);
    startDB();
}


function startDB() {
    var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    dataBase = indexedDB.open("main", 1);
    dataBase.onupgradeneeded = function (e) {
        active = dataBase.result;
        object = active.createObjectStore("product", {keyPath: 'id', autoIncrement: true});
        object.createIndex('by_title', 'title');
        object.createIndex('by_description', 'description');
        object.createIndex('by_sellerID', 'sellerID');
        object.createIndex('by_imgName', 'imgName');
        object.createIndex('by_datetime', 'datetime');
        object.createIndex('by_isClosed', 'isClosed');
        object.createIndex('by_price', 'price');
    };

    dataBase.onsuccess = function (e) {
        alert('Base de datos cargada correctamente');
        loadAll();
    };

    dataBase.onerror = function (e) {
        alert('Error cargando la base de datos');
    };

}


function loadAll() {

    var active = dataBase.result;
    var data = active.transaction("product", "readonly");
    var object = data.objectStore("product");

    var elements = [];

    object.openCursor().onsuccess = function (e) {
        var result = e.target.result;
        if (result === null)
            return;
        elements.push(result.value);
        result.continue();
    };

    data.oncomplete = function () {
        var outerHTML = '';
        for (var key in elements) {
            outerHTML += '<div class="panel panel-default">\n\
                          <!-- Default panel contents -->\n\
                <div class="panel-heading">' + elements[key].title + '</div>\n\
                <div class="panel-body">\n\
                    <p>' + elements[key].description + '</p>\n\
                </div>\n\
                <!-- List group -->\n\
                <ul class="list-group">\n\
                    <li class="list-group-item">' + elements[key].price + '€</li>\n\
                </ul>\n\
            </div>';

        }
        elements = [];
        document.querySelector("#productContainer").innerHTML = outerHTML;
    };
}


function handleOrder(e) {
    var elemento = e.target;
    if (elemento === document.getElementById("orderByPrice")) { //-----Price----------
        orderBy = "price";
    } else if (elemento === document.getElementById("orderByDate")) { //-----Date----------
        orderBy = "date";
    } else if (elemento === document.getElementById("orderByDefault")) { //-----Default----------
        orderBy = "default";
    } else if (elemento === document.getElementById("orderDescending")) { //-----Default----------
        orderDescending = true;
    } else if (elemento === document.getElementById("orderAscending")) { //-----Default----------
        orderDescending = false;
    }
    order();
}


function order() {
    var active = dataBase.result;
    var data = active.transaction("product", "readonly");
    var index;
    var elements = [];

    if (orderBy === "price") { //-----Price----------
        var object = data.objectStore("product");
        index = object.index("by_price");
    } else if (orderBy === "date") { //-----Date----------
        var object = data.objectStore("product");
        index = object.index("by_datetime");
    } else if (orderBy === "default") { //-----Default----------
        index = data.objectStore("product");
    }

    index.openCursor().onsuccess = function (e) {
        var result = e.target.result;
        if (result === null)
            return;
        elements.push(result.value);
        result.continue();
    };
    
    if (!orderDescending) elements = reverseItems(elements);

    data.oncomplete = function () {
        var outerHTML = '';
        for (var key in elements) {
            outerHTML += '<div class="panel panel-default">\n\
                          <!-- Default panel contents -->\n\
                <div class="panel-heading">' + elements[key].title + '</div>\n\
                <div class="panel-body">\n\
                    <p>' + elements[key].description + '</p>\n\
                </div>\n\
                <!-- List group -->\n\
                <ul class="list-group">\n\
                    <li class="list-group-item">' + elements[key].price + '€</li>\n\
                </ul>\n\
            </div>';

        }
        elements = [];
        document.querySelector("#productContainer").innerHTML = outerHTML;
    };
}

function reverseItems(elements) {
    var reversed = [];
    var items = [];
    var pos = 0;
    for (var item in elements) {
        items[pos] = item;
        pos++;
    }
    for (i = 0; i < items.length; i++) { 
        reversed[i] = items[items.length - 1 - i];
    }
    return reversed;
}