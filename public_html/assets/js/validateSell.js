
window.addEventListener("load", setup, false);

function setup() {

    document.sellForm.addEventListener("input",
            controlar, false);
    document.getElementById("submit").addEventListener("click",
            enviar, false);
}

function validacion(e) {
    var elemento = e.target;
    elemento.style.background = '#FFDDDD';
}

function controlar(e) {
    var elemento = e.target;
    if (elemento.validity.valid) {
        elemento.style.background = '#FFFFFF';
    } else {
        elemento.style.background = '#FFDDDD';
    }
}

function enviar() {
    var valido = document.sellForm.checkValidity();
    if (valido) {
        document.getElementById("sellForm").submit();
    }
}