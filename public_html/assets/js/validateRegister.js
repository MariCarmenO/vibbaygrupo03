

window.addEventListener("load", setup, false);

function setup() {

    document.registerForm.addEventListener("input",
            controlar, false);
    document.getElementById("submit").addEventListener("click",
            enviar, false);
    document.getElementById("confirmPassword").addEventListener("change", controlarPwd, false);
    document.getElementById("password").addEventListener("change", controlarPwd, false);
    document.registerForm.formValidation();
}

function validacion(e) {
    var elemento = e.target;
    elemento.style.background = '#FFDDDD';
}

function controlar(e) {
    var elemento = e.target;
    if (elemento.validity.valid) {
        elemento.style.background = '#FFFFFF';
    } else {
        elemento.style.background = '#FFDDDD';
    }
}

function controlarPwd(e) {
    var elemento = e.target;
    if (document.getElementById("password").value === "" ||
            document.getElementById("confirmPassword").value === "")
        return;
    if (equalPasswords()) {
        elemento.style.background = '#FFFFFF';
        document.getElementById("confirmPasswordErrorLabel").innerHTML = "";
    } else {
        elemento.style.background = '#FFDDDD';
        document.getElementById("confirmPasswordErrorLabel").innerHTML = "The passwords do not match";
    }
}

function equalPasswords() {
    var a = document.getElementById("password").value;
    var b = document.getElementById("confirmPassword").value;  
    return document.getElementById("password").value === document.getElementById("confirmPassword").value;
}

function enviar() {
    var valido = document.registerForm.checkValidity() && equalPasswords();
    if (valido) {
        document.registerForm.submit();
    }
}